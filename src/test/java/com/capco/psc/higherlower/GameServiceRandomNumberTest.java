package com.capco.psc.higherlower;

import com.capco.psc.higherlower.dto.Guess;
import com.capco.psc.higherlower.model.Game;
import com.capco.psc.higherlower.model.GameStatus;
import com.capco.psc.higherlower.repository.GameRepository;
import com.capco.psc.higherlower.service.GameService;
import com.capco.psc.higherlower.service.RandomGenerationService;
import lombok.extern.java.Log;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.annotation.Repeat;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
@Log
public class GameServiceRandomNumberTest {

    @Autowired
    private GameService gameService;

    @Test
    @Repeat(100)
    public void given_existing_game_when_guess_expect_different_generated_number_as_previous_number() {
        Game game = gameService.startNewGame();

        Integer previousNumber=game.getLast();

        game = gameService.guessNextNumber(game.getId(), Guess.HIGHER);

        Assert.assertNotEquals(previousNumber, game.getLast());
        log.info(previousNumber+"!="+game.getLast());
    }


}
