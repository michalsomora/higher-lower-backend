package com.capco.psc.higherlower;

import com.capco.psc.higherlower.dto.Guess;
import com.capco.psc.higherlower.model.Game;
import com.capco.psc.higherlower.model.GameStatus;
import com.capco.psc.higherlower.repository.GameRepository;
import com.capco.psc.higherlower.service.GameService;
import com.capco.psc.higherlower.service.RandomGenerationService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import javax.transaction.Transactional;

@RunWith(SpringRunner.class)
@SpringBootTest
@DataJpaTest
@Transactional
public class GameServiceTest {

    private static final Integer RANDOM_NUMBER_EXAMPLE=6;

    @Autowired
    private GameService gameService;

    @MockBean
    private RandomGenerationService randomGenerationServiceMock;

    @Autowired
    private GameRepository gameRepository;

    @Before
    public void init() {
        Mockito.when(randomGenerationServiceMock.getRandomNumber()).thenReturn(RANDOM_NUMBER_EXAMPLE);
    }

    @Test
    public void given_empty_db_when_game_start_called_expects_game_entity_saved() {

        Game game = gameService.startNewGame();

        Assert.assertNotNull(game);
        Assert.assertNotNull(game.getId());
        Assert.assertEquals(RANDOM_NUMBER_EXAMPLE, game.getLast());
        Assert.assertEquals(0, game.getRound());
        Assert.assertEquals(GameStatus.PENDING, game.getStatus());

    }

    @Test
    public void given_existing_new_game_when_guess_called_HIGHER_expects_correct_state_of_game() {

        Game game = gameService.startNewGame();

        Mockito.when(randomGenerationServiceMock.getRandomNumber()).thenReturn(RANDOM_NUMBER_EXAMPLE+1);

        game = gameService.guessNextNumber(game.getId(), Guess.HIGHER);

        Assert.assertEquals(1, game.getRound());
        Assert.assertEquals(GameStatus.PENDING, game.getStatus());
        Assert.assertEquals(new Integer(RANDOM_NUMBER_EXAMPLE+1), game.getLast());

    }

    @Test
    public void given_existing_new_game_when_guess_called_LOWER_expects_correct_state_of_game() {

        Game game = gameService.startNewGame();

        Mockito.when(randomGenerationServiceMock.getRandomNumber()).thenReturn(RANDOM_NUMBER_EXAMPLE+1);

        game = gameService.guessNextNumber(game.getId(), Guess.LOWER);

        Assert.assertEquals(0, game.getRound());
        Assert.assertEquals(GameStatus.LOOSE, game.getStatus());
        Assert.assertEquals(new Integer(RANDOM_NUMBER_EXAMPLE+1), game.getLast());

    }

    @Test
    public void given_existing_new_game_with_round_4_when_succesfull_guess_called_expects_WIN_state() {

        Game game = gameService.startNewGame();
        game.setRound(4);
        gameRepository.saveAndFlush(game);

        Mockito.when(randomGenerationServiceMock.getRandomNumber()).thenReturn(RANDOM_NUMBER_EXAMPLE+1);

        game = gameService.guessNextNumber(game.getId(), Guess.HIGHER);

        Assert.assertEquals(5, game.getRound());
        Assert.assertEquals(new Integer(RANDOM_NUMBER_EXAMPLE+1), game.getLast());
        Assert.assertEquals(GameStatus.WIN, game.getStatus());

    }


}
