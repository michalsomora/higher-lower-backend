package com.capco.psc.higherlower;

import com.capco.psc.higherlower.controller.GameController;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;


@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@WebMvcTest
public class GameControllerTest {



    @Autowired
    private GameController gameController;

    @Autowired
    private TestRestTemplate testRestTemplate;

    @Test
    public void when_game_started_expect_correct_response() {
        assertThat(
                this.testRestTemplate.getForObject( "/api/game/start",
                String.class)).contains("\"round\":0").contains("\"status\":\"PENDING\"");
    }
}
