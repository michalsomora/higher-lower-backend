package com.capco.psc.higherlower.model;

import lombok.Data;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.Date;

import static javax.persistence.TemporalType.TIMESTAMP;

@Entity
@Data
@EntityListeners(AuditingEntityListener.class)
public class Game {

    @Id
    @GeneratedValue
    private long id;

    @Column(name = "round")
    private int round = 0;

    private Integer last;

    private String name;

    @CreatedDate
    @Temporal(TIMESTAMP)
    private Date date;

    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private GameStatus status = GameStatus.PENDING;
}
