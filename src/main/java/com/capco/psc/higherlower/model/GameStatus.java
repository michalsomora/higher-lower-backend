package com.capco.psc.higherlower.model;

public enum GameStatus {
    PENDING, LOOSE, WIN
}
