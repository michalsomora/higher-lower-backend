package com.capco.psc.higherlower.config;

import com.capco.psc.higherlower.dto.GameRules;
import com.capco.psc.higherlower.service.RandomGenerationService;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties
public class GameConfig {

    @Bean
    @ConfigurationProperties(prefix = "game.rules")
    public GameRules gameRules() {
        return new GameRules();
    }

    @Bean
    @ConditionalOnMissingBean
    public RandomGenerationService getAutoConfiguredRandomGenerationService() {
        return new RandomGenerationService() {
            @Override
            public int getRandomNumber() {
                return 6;
            }
        };
    }
}

