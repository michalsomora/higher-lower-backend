package com.capco.psc.higherlower.service;

public interface RandomGenerationService {

    public int getRandomNumber();
}
