package com.capco.psc.higherlower.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Service
public class SimpleRandomGenerationService implements RandomGenerationService{

    @Autowired
    private Environment environment;

    @Override
    public int getRandomNumber() {
        return environment.getProperty("simple.randomNumber", Integer.class);
    }
}
