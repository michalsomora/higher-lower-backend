package com.capco.psc.higherlower.service.mapper;

import com.capco.psc.higherlower.dto.GameDto;
import com.capco.psc.higherlower.model.Game;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper(componentModel = "spring")
public interface GameMapper {

    GameDto mapGameToDto(Game game);
}
