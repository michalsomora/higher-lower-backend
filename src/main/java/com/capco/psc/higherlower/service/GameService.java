package com.capco.psc.higherlower.service;

import com.capco.psc.higherlower.dto.GameRules;
import com.capco.psc.higherlower.dto.Guess;
import com.capco.psc.higherlower.exception.GameInWrongStateException;
import com.capco.psc.higherlower.exception.GameNotFoundException;
import com.capco.psc.higherlower.model.Game;
import com.capco.psc.higherlower.model.GameStatus;
import com.capco.psc.higherlower.repository.GameRepository;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Log
@Transactional
public class GameService {

    @Autowired
    private GameRules gameRules;

    @Autowired
    private RandomGenerationService randomGenerationService;

    @Autowired
    private GameRepository gameRepository;

    public Game startNewGame() {
        return startNewGame(null);
    }

    public Game startNewGame(String name) {
        Game game = new Game();
        game.setName(name);
        game.setLast(randomGenerationService.getRandomNumber());

        gameRepository.saveAndFlush(game);

        return game;
    }

    public Game guessNextNumber(long gameId, Guess guess) {
        Game game = gameRepository.findOne(gameId);

        if (game==null) {
            throw new GameNotFoundException("Game id:"+gameId);
        }
        if (game.getStatus()!= GameStatus.PENDING) {
            throw new GameInWrongStateException("Game id:"+gameId);
        }

        if (game.getRound()>gameRules.getMaxRound()) {
            throw new GameInWrongStateException("Game id:"+gameId);
        }

        Integer nextRandomNumber = randomGenerationService.getRandomNumber();
        while (game.getLast()==nextRandomNumber) {
            nextRandomNumber = randomGenerationService.getRandomNumber();
            log.info("next random number2="+nextRandomNumber);
        }


        if (nextRandomNumber.compareTo(game.getLast()) == guess.getValue()) {
            game.setRound(game.getRound()+1);
        } else {
            game.setStatus(GameStatus.LOOSE);
        }

        if (game.getRound()==gameRules.getMaxRound()) {
            game.setStatus(GameStatus.WIN);
        }

        game.setLast(nextRandomNumber);

        gameRepository.saveAndFlush(game);

        return game;

    }

    public List<Game> getGames() {
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }

        return gameRepository.findAll();
    }

    public List<Game> getGamesLike(String name) {
        return gameRepository.findByNameStartingWith(name);
    }
}
