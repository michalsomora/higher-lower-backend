package com.capco.psc.higherlower.repository;

import com.capco.psc.higherlower.model.Customer;
import com.capco.psc.higherlower.model.Game;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;



public interface GameRepository extends JpaRepository<Game, Long>{
    List<Game> findByNameStartingWith(String nameLike);
}
