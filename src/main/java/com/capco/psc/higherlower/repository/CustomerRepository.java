package com.capco.psc.higherlower.repository;

import com.capco.psc.higherlower.model.Customer;
import com.capco.psc.higherlower.model.Game;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource
public interface CustomerRepository extends JpaRepository<Customer, Long>{
}
