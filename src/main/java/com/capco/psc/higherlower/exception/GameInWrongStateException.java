package com.capco.psc.higherlower.exception;

public class GameInWrongStateException extends RuntimeException {

    public GameInWrongStateException(String message) {
        super(message);
    }
}
