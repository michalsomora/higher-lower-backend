package com.capco.psc.higherlower.dto;

import com.capco.psc.higherlower.model.GameStatus;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class GameDto {

    private long id;
    private int guess;
    private GameStatus status;
    private int round;
    private int last;
    private String name;
    private Date date;
}
