package com.capco.psc.higherlower.dto;

import lombok.Data;

@Data
public class GameRules {

    private int maxRound;

    private int maxNumber;

    private int minNumber;
}
