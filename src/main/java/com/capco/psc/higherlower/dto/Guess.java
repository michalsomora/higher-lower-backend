package com.capco.psc.higherlower.dto;

import java.util.stream.Stream;

public enum Guess {

    HIGHER(1),LOWER(-1);

    private int value;

    Guess(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public static Guess fromValue(int value) {
        for (Guess guess: values()) {
            if (guess.value == value) {
                return guess;
            }
        }
        throw new IllegalArgumentException("Invalid value " + value);
    }

}
