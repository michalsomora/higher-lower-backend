package com.capco.psc.higherlower.controller;

import com.capco.psc.higherlower.dto.GameRules;
import com.capco.psc.higherlower.dto.GameDto;
import com.capco.psc.higherlower.dto.Guess;
import com.capco.psc.higherlower.model.Game;
import com.capco.psc.higherlower.service.GameService;
import com.capco.psc.higherlower.service.mapper.GameMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("api/game")
@CrossOrigin(origins = "*")
public class GameController {

    @Autowired
    private GameRules gameRules;

    @Autowired
    private GameService gameService;

    @Autowired
    private GameMapper gameMapper;

    @RequestMapping(path = "/start/", method = RequestMethod.GET)
    public GameDto startNewGame(@RequestParam(defaultValue = "defaultName") String name) {
        return gameMapper.mapGameToDto(gameService.startNewGame(name));
    }

    @RequestMapping(path = "/start", method = RequestMethod.GET)
    public GameDto startNewGame() {
        return gameMapper.mapGameToDto(gameService.startNewGame(null));
    }

    @RequestMapping(path = "/guess", method = RequestMethod.POST)
    public GameDto guessNextNumber(@RequestBody GameDto gameDto) {
        Game game = gameService.guessNextNumber(gameDto.getId(), Guess.fromValue(gameDto.getGuess()));

        return gameMapper.mapGameToDto(game);
    }

    @RequestMapping(path = "/", method = RequestMethod.GET)
    public List<GameDto> getGames() {
        return gameService.getGames().stream().map(g -> {
            return gameMapper.mapGameToDto(g);
        }).collect(Collectors.toList());
    }

    @RequestMapping(path = "/rules", method = RequestMethod.GET)
    public GameRules getGameRules() {
        return gameRules;
    }

    @RequestMapping(path = "/like/{name}", method = RequestMethod.GET)
    public List<Game> getGames(@PathVariable String name) {
        return gameService.getGamesLike(name);
    }
}
